import { useMemo } from "react";
import { createStore, applyMiddleware, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

export enum WrapperType {
  Artist = "artist",
  Collection = "collection",
  Track = "track"
}

export interface Result {
  wrapperType: WrapperType;
  artistName: string;
  collectionName?: string;
  trackName?: string;
  trackId?: string;
  collectionId?: string;
  artistId?: string;
  artistLinkUrl?: string;
  collectionViewUrl?: string;
  trackViewUrl?: string;
}

export interface StateProps {
  page: number;
  results: Result[];
}

let store: Store<StateProps>;

const initialState: StateProps = {
  page: 10,
  results: []
};

const reducer = (state = initialState, action: { type: string, payload?: any }) => {
  switch (action.type) {
    case "LOAD_RESULTS":
      return {
        ...state,
        results: action.payload.results,
        page: action.payload.page
      };
    case "LOAD_MORE":
      return {
        ...state,
        page: state.page + 10
      };
    default:
      return state;
  }
};

function initStore(preloadedState = initialState) {
  return createStore(reducer, preloadedState, composeWithDevTools(applyMiddleware(thunk)));
} 

export const initializeStore = (preloadedState: StateProps) => {
  let _store = store ?? initStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === "undefined") return _store;
  // Create the store once in the client
  if (!store) {
    store = _store as unknown as Store;
  }

  return _store;
};

export function useStore(initialState: StateProps) {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
}
