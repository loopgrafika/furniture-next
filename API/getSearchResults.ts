import axios, { AxiosResponse } from "axios";
import { Result } from "store";

export type APIResponse = Promise<AxiosResponse<{ results: Result[] }>>;

export const getSearchResults = (term: string, page: number): APIResponse =>
  axios.get(`https://itunes.apple.com/search?term=${term.replace(/\s/g, '+')}&media=music&entity=musicArtist,album,song&limit=${page}`);

