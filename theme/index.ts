import { createMuiTheme } from "@material-ui/core/styles";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#e91e63"
    },
    secondary: {
      main: "#bdbdbd"
    },
    background: {
      default: "#fafafa"
    }
  }
});

export default theme;
