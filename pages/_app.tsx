import { AppProps } from 'next/app';
import { Provider as ReduxProvider } from 'react-redux';

import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { StateProps, useStore } from '../store'
import theme from '../theme';
import { Store } from 'redux';

export default function App({ Component, pageProps }: AppProps) {
  const store: Store<StateProps, { type: string, payload?: any }> = useStore(pageProps.initialReduxState)
  return (
    <ReduxProvider store={store}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...pageProps} />
      </ThemeProvider>
    </ReduxProvider>
  );
}