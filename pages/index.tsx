import React, { useState } from "react";
import Head from "next/head";
import { GetStaticProps } from "next";
import { useSelector } from "react-redux";
import { Grid, List, Input, Card, Typography } from "@material-ui/core";

import InfiniteScroll from "react-infinite-scroller";

import { useActions } from "../hooks";
import Layout from "../components/layout";
import { Result, StateProps } from "../store";
import ResultCell from "../components/cell";
import { APIResponse, getSearchResults } from "../API/getSearchResults";

const renderList = (results: Result[]) => results.map((result: Result) => <ResultCell data={result} key={result.trackId || result.collectionId || result.artistId}/>);

export default function Home({ initialTerm, pageTitle, api = getSearchResults }: { initialTerm: string; pageTitle: string , api: (term?: string, page?: number) => APIResponse}) {
  const results: Result[] = useSelector((state: StateProps) => state.results);

  const { term, loadItems, loadMoreItems, page } = useActions(initialTerm, api);

  return (
    <Layout>
      <Head>
        <title>{pageTitle}</title>
      </Head>
      <Grid container spacing={3} direction="column">
        <Grid item container justify="center">
          <Typography variant="h2" component="h1" align="center" color="secondary">
            {pageTitle}
          </Typography>
        </Grid>
        <Grid item container justify="center">
          <Input placeholder="Search…" inputProps={{ "aria-label": "search" }} value={term} onChange={loadItems} />
        </Grid>
        <Grid item container alignItems="center" direction="column">
          {!!results?.length ? (
            <Card>
              <List>
                  <InfiniteScroll
                    loadMore={loadMoreItems}
                    hasMore={page <= 100}
                    initialLoad={false}
                    threshold={10}
                  >
                    {renderList(results)}
                  </InfiniteScroll>
              </List>
            </Card>
          ) : (
            <Typography color="secondary">No results</Typography>
          )}
        </Grid>
      </Grid>
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      initialTerm: "Queen",
      pageTitle: "Apple Music Search"
    }
  };
};
