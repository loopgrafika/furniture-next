module.exports = {
  setupFilesAfterEnv: ["./setupTests.js"],
  moduleNameMapper: {
    "^@components(.*)$": "<rootDir>/components$1",
    "^@pages(.*)$": "<rootDir>/pages$1",
    "^@hooks(.*)$": "<rootDir>/hooks$1",
    "^@actions(.*)$": "<rootDir>/actions$1"
  }
};
