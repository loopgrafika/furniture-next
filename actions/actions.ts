
import { Dispatch } from "redux";

import { APIResponse } from "../API/getSearchResults";
import { Result } from "../store";

const loadResults = (results: Result[], page: number) => ({
  type: "LOAD_RESULTS",
  payload: { results, page }
});

export const loadMore = () => ({
  type: "LOAD_MORE"
});

export const reset = () => ({
  type: "RESET"
});

export const fetchResults = (term: string, page: number, getSearchResults: (term: string, page: number) => APIResponse) => {
  return async (dispatch: Dispatch<{ type: string }>) => {
    const response = await getSearchResults(term, page);    
    return dispatch(loadResults(response.data.results, page));
  };
};

export const moreResults = () => {
  return async (dispatch: Dispatch<{ type: string }>) => {
    return dispatch(loadMore());
  };
};

export const resetPage = () => {
  return async (dispatch: Dispatch<{ type: string }>) => {
    return dispatch(reset());
  };
};