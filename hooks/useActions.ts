import { useDispatch, useSelector } from "react-redux";

import { useCallback, useEffect, useState } from "react";
import { StateProps } from "../store";
import { fetchResults, moreResults, resetPage } from "../actions";
import { APIResponse } from "API/getSearchResults";

export interface HomeActionsAndValues {
  term: string;
  page: number;
  loadItems: (event: React.ChangeEvent<HTMLInputElement>) => void;
  loadMoreItems: () => void;
}

export function useActions(initialTerm: string, api: (term: string, page: number) => APIResponse): HomeActionsAndValues {
  const dispatch = useDispatch();

  const page: number = useSelector((state: StateProps) => state.page);

  const [term, setTerm] = useState(initialTerm);

  useEffect(() => {
    dispatch(fetchResults(term, page, api))
  }, [])

  const loadItems = useCallback(
    e => {
      setTerm(e.target.value);
      dispatch(fetchResults(e.target.value, 10, api));
    },
    [dispatch, term, page]
  );

  const loadMoreItems = useCallback(async () => {
    dispatch(moreResults());
    dispatch(fetchResults(term, page + 10, api));
  }, [dispatch, page, term])


  return {
    page,
    term,
    loadItems,
    loadMoreItems
  }
}