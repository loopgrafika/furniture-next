import { ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import { Group, Album, Audiotrack } from "@material-ui/icons";
import { ReactElement, useCallback } from "react";
import { Result, WrapperType } from "../store";

export default function ResultCell({ data }: { data: Result }): React.ReactElement {
  const {
    wrapperType,
    artistName,
    collectionName,
    trackName,
    artistLinkUrl,
    collectionViewUrl,
    trackViewUrl
  } = data;

  const handleClick = useCallback((url: string) => {
    history.pushState({ page: "result" }, "Result", url);
    document.location.replace(url);
  }, []);
  let component: ReactElement;
  let secondary: string;
  switch (wrapperType) {
    case WrapperType.Artist:
      component = <Group />;
      break;
      case WrapperType.Collection:
        component = <Album />;
        secondary = artistName;
      break;
    default:
      component = <Audiotrack />;
      secondary = `${collectionName} – ${artistName}`;
  }
      return (
        <ListItem button onClick={() => handleClick(trackViewUrl || collectionViewUrl || artistLinkUrl)}>
          <ListItemIcon>
            {component}
          </ListItemIcon>
          <ListItemText primary={trackName || collectionName || artistName} secondary={secondary}/>
        </ListItem>
      );
}
