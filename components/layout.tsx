import Head from "next/head";

export const siteTitle = "Apple Music Search";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content="A simple Apple Music search bult on Next.js" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
      </Head>
      <main>{children}</main>
    </div>
  );
}
