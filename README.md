## Install app
with npm
`npm install`
with yarn
`yarn`

## Run app
`npm run dev`
or
`yarn dev`

## Run tests
`npm run test`
or
`yarn test`

## Build app
`npm run build`
or
`yarn build`


