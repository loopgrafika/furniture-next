// test-utils.js
import { render } from "@testing-library/react";
import { Provider as ReduxProvider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core/styles';

import { useStore } from '../store'
import theme from '../theme';

const Providers = ({ children }) => {
  const store = useStore({})
  
  return (
    <ReduxProvider store={store}>
      <ThemeProvider theme={theme}>
        {children}
      </ThemeProvider>
    </ReduxProvider>
  );
};

const customRender = (ui, options = {}) => render(ui, { wrapper: Providers, ...options });

export * from "@testing-library/react";

export { customRender as render };
