// test/pages/index.test.js

import React from "react";
import { render, screen, fireEvent } from "./test-utils";
import Home from "@pages/index";
import { APIResponse } from '../API/getSearchResults';
import { Result, WrapperType } from "../store";

const mockResults: Result[] = [
      {
        wrapperType: WrapperType.Artist,
        artistName: 'Queen',
        collectionName: 'string',
        artistId: '1234',
        artistLinkUrl: 'link/to/queen',
      },
      {
        wrapperType: WrapperType.Collection,
        artistName: 'Queen',
        collectionName: 'Best of Queen',
        collectionId: '5678',
        artistId: 'string',
        artistLinkUrl: 'link/to/queen',
        collectionViewUrl: 'link/to/queen/best_of_queen',
      },
      {
        wrapperType: WrapperType.Track,
        artistName: 'Queen',
        collectionName: 'Best of Queen',
        trackName: 'Bohemian Rhapsody',
        trackId: '7890',
        collectionId: '5678',
        artistId: '1234',
        artistLinkUrl: 'link/to/queen',
        collectionViewUrl: 'link/to/queen/best_of_queen',
        trackViewUrl: 'link/to/queen/best_of_queen/bohemian_rhapsody'
      }
    ]
const mockResponse = {
  data: {
    results: mockResults
  },
  status: 200, statusText: '', headers: '', config: {}
}
const mockEmptyResponse = {
  data: {
    results: []
  },
  status: 200, statusText: '', headers: '', config: {}
}
// 
describe("App", () => {
  const mockApi = (term: string): APIResponse => {
    return term === "Queen" ? Promise.resolve(mockResponse) : Promise.resolve(mockEmptyResponse);
  }; 
  it("should render the page with no results", async () => {
    render(<Home initialTerm="" pageTitle="Test Title" api={mockApi} />);

    const heading = screen.getByText("Test Title");
    const input = screen.getByLabelText("search");

    fireEvent.change(input, { target: { value: "" }});

    const noResults = await screen.findByText("No results");
    
    expect(heading).toBeInTheDocument();
    expect(noResults).toBeInTheDocument();
  });

  it("should load results when typing a search term", async () => {
    render(<Home initialTerm="Test Term" pageTitle="Test Title" api={mockApi} />);

    const input = screen.getByLabelText("search");

    fireEvent.change(input, { target: { value: "Queen" }});

    const artist = await screen.findByText("Queen");
    const album = await screen.findByText("Best of Queen");
    const track = await screen.findByText("Bohemian Rhapsody");

    expect(input).toBeInTheDocument();
    expect(artist).toBeInTheDocument();
    expect(album).toBeInTheDocument();
    expect(track).toBeInTheDocument();
  });
});
